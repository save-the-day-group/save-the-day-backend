const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var ongSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    whatsapp: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: true,
    },
    city: {
        type: String,
        required: true
    },
    uf: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    },
    password: {
        type: String,
        required: true,
       // select: false
    }
});

module.exports = mongoose.model('OngModel', ongSchema);