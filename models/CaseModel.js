const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var caseSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    imageUrl: {
        type: String
    },
    ong_id: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('CaseModel', caseSchema);