const mongoose = require('mongoose');
const Schema = mongoose.Schema;

var caseStateSchema = new Schema({
    case_id: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    description: {
        type: String
    }
});

module.exports = mongoose.model('CaseStateModel', caseStateSchema);