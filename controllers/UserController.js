const connection = require('../database/connection');
const User = require('../models/UserModel');
const bcrypt = require('bcrypt');

module.exports = {
    create(req, res) {
        const { name, email, password } = req.body;
        bcrypt.hash(password, 10, function(err, hash) {
            const user = new User({
                name,
                email,
                password: hash
            });
            user.save()
            .then(resp => {
                return res.status(201).json({"data": resp});
            })
            .catch(err => {
                return res.status(400).json({"error": err});
            })  
        })
    },
    getById(req, res) {
        const { id } = req.params;
        User.findById(id)
        .then(resp => {
            return res.status(200).json({"data": resp})
        })
        .catch(err => {
            return res.status(400).json({"error": err})
        });
    },

    login(req, res) {
        const { email, password } = req.body;
        User.findOne({ email:email })
        .then(user => {
            if(user.active == true) {
                bcrypt.compare(password, user.password, function(err, result) {
                    if(result == true) {
                        return res.status(200).json({"data": {
                            "name": user.name,
                            "email": user.email,
                            "_id": user._id,
                            "active": user.active
                        }})
                    } else {
                        return res.status(400).json({"error": "Invalid credentials"})
                    }
                })
            }
        })
        .catch(err => {
            return res.status(400).json({"error": "Invalid credentials..."})
        });

    },

    update(req, res) {
        const { name, email } = req.body;
        const token = req.headers.authorization;
        const { id } = req.params;
        User.findByIdAndUpdate(id, {name: name, email: email}, {
            new: true
        })
        .then(resp => {
            return res.status(201).json(resp);
        })
        .catch(err => {
            return res.status(400).json(err);
        }); 
    },

    delete(req, res) {

    },

    changePassword(req, res) {
        const { password } = req.body;
        const { id } = req.params;
        User.findById(id)
        .then(user => {
            bcrypt.hash(password, 10, function(err, hash) {
                user.password = hash;
                user.save();
                return res.status(201).json({"data": user})
            })
        })
        .catch(err =>{
            return res.status(400).json({"error": err})
        })
    }
}