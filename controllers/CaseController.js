const connection = require('../database/connection');

const Case = require('../models/CaseModel');

module.exports = {
    create(req, res) {
        const { title, description, imageUrl, ong_id } = req.body;
       // const id = req.headers.authorization;
        const cas = new Case({
            title,
            description,
            imageUrl,
            ong_id
        });
        cas.save()
        .then(resp => {
            return res.status(201).json({"data":resp})
        })
        .catch(err => {
            return res.status(400).json({"error":err})
        });
    },
    // all cases to display in the app
    index(req, res) {
        Case.find()
        .then(resp=> {
            return res.status(200).json({"data": resp})
        })
        .catch(err => {
            return res.status(400).json({"error": err})
        })
    },

    listCasesByOng(req, res) {
        const ong_id = req.headers.authorization;
        const { id } = req.params
        if(!ong_id) {
            return res.json({'error': 'Token is required'});
        } else {
            Case.find({ong_id: id})
            .then(resp => {
                return res.status(200).json({"data":resp})
            })
            .catch(err => {
                return res.status(400).json({"error": err})
            })
        }
    },
    getCase(req, res) {
        const { id } = req.params;
        if(!id) {
            return res.json({'error': 'Invalid param'});
        }
        Case.findById(id)
        .then(resp=> {
            return res.status(200).json({"data": resp})
        })
        .catch(err => {
            return res.status(400).json({"error": err})
        });
    },
    // todo: soft delete of cases
    delete(req, res) {
        const ong_id = req.headers.authorization;
        const { id } = req.params;
        if(!ong_id) {
            return res.json({'error': 'Invalid identification'});
        } else {
            Case.findByIdAndDelete(id)
            .then(resp => {
                console.log('resp', resp);
                return res.status(200).json({"data": "Case deleted successfully"})
            })
            .catch(err => {
                return res.status(400).json({"error": err})
            });
        }
    }
}