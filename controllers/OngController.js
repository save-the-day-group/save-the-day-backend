const connection = require('../database/connection');
const Ong = require('../models/OngModel');

const bcrypt = require('bcrypt');

module.exports = {
    create(req, res) {
        const { name, email, whatsapp, city, uf, description, password } = req.body;
        bcrypt.hash(password, 10, function(err, hash) {
            const ong = new Ong({
                name, 
                email,
                whatsapp,
                city,
                uf,
                description,
                password: hash
            });
            ong.save().then(ong => {
                return res.status(201).json({"data": {
                    "_id": ong._id,
                    "name": ong.name,
                    "description": ong.description,
                    "email": ong.email,
                    "whatsapp": ong.whatsapp,
                    "city": ong.city,
                    "uf": ong.uf,
                    "active": ong.active
                }});
            })
            .catch(err => {
                if(err.code === 11000 && err.keyValue.email) {
                    return res.status(400).json({"error": "This email already exists"});
                }
                if(err.code === 11000 && err.keyValue.whatsapp) {
                    return res.status(400).json({"error": "This WhatsApp number already exists"});
                }
            });

        })
    },
    //to be displayed in the app only
    index(req, res) {
        Ong.find({active: true})
        .then(ongs => {
            return res.status(200).json({"data": ongs});
        })
        .catch(err => {
            return res.status(400).json(err);
        });
    },

    update(req, res) {
        const { name, email, whatsapp, city, uf, description } = req.body;
        const ong_id = req.headers.authorization;
        const { id } = req.params;
        Ong.findByIdAndUpdate(id, req.body, {
            new: true
        })
        .then(resp => {
            return res.status(201).json(resp);
        })
        .catch(err => {
            return res.status(400).json(err);
        }); 
    },

    getOng(req, res) {
        const ong_id = req.headers.authorization;
        const { id } = req.params;
        Ong.findById(id)
        .then(resp => {
            res.status(200).json({
                "data": resp
            })
        })
        .catch(err => {
            res.status(400).json({"error": err})
        });
    },

    delete(req, res) {
        const token = req.headers.authorization;
        const { id } = req.params;
        
        if(!token) {
            return res.status(400).json({'error': 'Invalid token'});
        } else {
            Ong.findByIdAndUpdate(id, {active: false}, {new: true})
            .then(resp => {
                console.log('resp', resp);
                return res.status(200).json({"data": resp})
            })
            .catch(err => {
                return res.status(400).json({"error": err})
            });
        }
    },
    login(req, res) {
        const { email, password } = req.body;
        Ong.findOne({email: email})
        .then(ong => {
            if(ong.active == true) {
                bcrypt.compare(password, ong.password, (err, result) => {
                    if(result == true) {
                        return res.status(200).json({"data": {
                            "name": ong.name,
                            "email": ong.email,
                            "_id": ong._id,
                            "active": ong.active,
                            "whatsapp": ong.whatsapp,
                            "description": ong.description,
                            "city": ong.city,
                            "uf": ong.uf
                        }})
                    } else {
                        return res.status(400).json({"error": "Invalid credentials"})
                    }
                })
            } else {
                return res.status(400).json({"error": "Ong is disabled"})
            }
        })
    },
    
    changePassword(req, res) {
        const { password } = req.body;
        const { id } = req.params;
        Ong.findById(id)
        .then(ong => {
            bcrypt.hash(password, 10, function(err, hash) {
                ong.password = hash;
                ong.save();
                return res.status(201).json({"data": ong})
            })
        })
        .catch(err =>{
            return res.status(400).json({"error": err})
        })
    }
}