const express = require('express');
const OngController = require('./controllers/OngController');
const CaseController = require('./controllers/CaseController');
const UserController = require('./controllers/UserController');
const { Segments, Joi, celebrate } = require('celebrate');

const routes = express.Router();

routes.get('/', (req, res) => {
    res.json({ message: "Hello From Backend"});
})

//Ongs endpoitns
routes.get('/ongs', OngController.index);

routes.post('/ongs/login', celebrate({
    [Segments.BODY]: Joi.object().keys({
        email: Joi.string().required().email(),
        password: Joi.string().required()
    })
}), OngController.login);

routes.post('/ongs', celebrate({
    [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        whatsapp: Joi.string().pattern(new RegExp('^[0-9]{11}$')).required(),
        city: Joi.string().required(),
        uf: Joi.string().required().length(2),
        description: Joi.string().required(),
        password: Joi.string().required()
    })
}), OngController.create);

routes.put('/ongs/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required().email(),
        whatsapp: Joi.string().pattern(new RegExp('^[0-9]{11}$')).required(),
        description: Joi.string().required(),
        city: Joi.string().required(),
        uf: Joi.string().required().length(2)
    })
}), OngController.update);

routes.get('/ong/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required()
    })
}), OngController.getOng);

routes.put('/ong/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required()
    })
}), OngController.delete);

routes.put('/ong/password/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
        password: Joi.string().required(),
    })
}), OngController.changePassword);

// Cases endpoints
routes.post('/cases', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.BODY]: Joi.object().keys({
        title: Joi.string().required(),
        description: Joi.string().required(),
        imageUrl: Joi.string(),
        ong_id: Joi.string().required()
    })
}), CaseController.create);

routes.get('/cases', CaseController.index);

routes.get('/cases-ongs/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required()
    })
}), CaseController.listCasesByOng);

routes.get('/case/:id', celebrate({
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required()
    })
}), CaseController.getCase);

routes.delete('/cases/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required()
    })
}), CaseController.delete);

//Users endpoints
routes.post('/user', celebrate({
    [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required(),
        password: Joi.string().required()
    })
}), UserController.create);

routes.get('/user/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required()
    })
}), UserController.getById);

routes.post('/login', celebrate({
    [Segments.BODY]: Joi.object().keys({
        email: Joi.string().required(),
        password: Joi.string().required()
    })
}), UserController.login);

routes.put('/users/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
        name: Joi.string().required(),
        email: Joi.string().required()
    })
}), UserController.update);

routes.put('/user/password/:id', celebrate({
    [Segments.HEADERS]: Joi.object({
        authorization: Joi.string().required()
    }).unknown(),
    [Segments.PARAMS]: Joi.object().keys({
        id: Joi.string().required(),
    }),
    [Segments.BODY]: Joi.object().keys({
        password: Joi.string().required(),
    })
}), UserController.changePassword);

module.exports = routes;